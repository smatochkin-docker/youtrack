# docker-youtrack

## Resources

* Exposed port: 8080
* Volume: /data/youtrack

## Usage

### Run youtrack

1. Create data container *youtrack-data*

    docker run --name youtrack-data smatochkin/youtrack true

1. Restore data from a backup (optional)

    docker run --rm -i \
      --volumes-from youtrack-data \
      smatochkin/youtrack \
      tar xz -C /data/youtrack/teamsysdata < ~/YouTrack_5.1_2014-09-22-09-00-00.tar.gz

1. Run youtrack on port 8080

    docker run -d --name youtrack --volumes-from youtrack-data -p 8080:8080 smatochkin/youtrack
