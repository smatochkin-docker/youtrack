FROM smatochkin/java:oracle7
MAINTAINER Sergey Matochkin <sergey@matochkin.com>

# Prepare data volume
RUN mkdir -p /data/youtrack/teamsysdata
VOLUME /data/youtrack

ADD ./etc /etc

# Install youtrack 
ENV YOUTRACK_VERSION 5.2.5-8823
RUN curl -LOs http://download.jetbrains.com/charisma/youtrack-$YOUTRACK_VERSION.jar

EXPOSE 8080

CMD ["/sbin/my_init"]
